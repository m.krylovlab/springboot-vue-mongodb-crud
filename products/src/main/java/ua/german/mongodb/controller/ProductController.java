package ua.german.mongodb.controller;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.german.mongodb.model.Product;
import ua.german.mongodb.repository.ProductsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "https://localhost:8081")
@RestController
@RequestMapping("/api")
public class ProductController {

    private static final Log LOGGER = LogFactory.getLog(ProductController.class);

    final ProductsRepository productsRepository;

    @Autowired
    public ProductController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts(
            @RequestParam(required = false) String title) {
        try {
            List<Product> listOfProducts = new ArrayList<>();

            if (title == null) {
                listOfProducts.addAll(productsRepository.findAll());
            } else {
                listOfProducts.addAll(productsRepository.findByTitleContaining(title));
            }

            return (listOfProducts.isEmpty())
                    ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
                    : new ResponseEntity<>(listOfProducts, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") String id) {
        Optional<Product> productData = productsRepository.findById(id);
        return productData.map(product -> new ResponseEntity<>(product, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /*@GetMapping("/products")
    public ResponseEntity<List<Product>> getProductByManufacturer(
            @RequestParam("manufacturer") String manufacturer) {

        List<Product> productData = new ArrayList<>(productsRepository.findByManufacturer(manufacturer));

        return (!productData.isEmpty())
                ? new ResponseEntity<>(productData, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }*/

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        try {
            Product newProduct = productsRepository
                    .save(new Product(
                            product.getTitle(),
                            product.getDescription(),
                            product.getManufacturer(),
                            product.getPrice()));

            return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(
            @PathVariable("id") String id, @RequestBody Product product) {
        Optional<Product> productData = productsRepository.findById(id);

        if (productData.isPresent()) {
            Product newProduct = productData.get();
            newProduct.setTitle(product.getTitle());
            newProduct.setDescription(product.getDescription());
            newProduct.setManufacturer(product.getManufacturer());
            newProduct.setPrice(product.getPrice());
            return new ResponseEntity<>(productsRepository.save(newProduct), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/products")
    public ResponseEntity<HttpStatus> deleteAllProducts() {
        try {
            productsRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            LOGGER.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("products/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable("id") String id) {
        try {
            productsRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            LOGGER.error(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
