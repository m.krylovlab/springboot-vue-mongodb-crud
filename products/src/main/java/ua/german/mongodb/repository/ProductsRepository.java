package ua.german.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.german.mongodb.model.Product;

import java.util.List;

@Repository
public interface ProductsRepository extends MongoRepository<Product, String> {
    List<Product> findByTitleContaining(String title);
    List<Product> findByManufacturer(String manufacturer);
}
