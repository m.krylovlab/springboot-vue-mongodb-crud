package ua.german.mongodb.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "product")
@Data
public class Product {
    @Id
    private String id;

    private String title;
    private String description;
    private String manufacturer;
    private BigDecimal price;

    public Product(String title, String description, String manufacturer, BigDecimal price) {
        this.title = title;
        this.description = description;
        this.manufacturer = manufacturer;
        this.price = price;
    }
}
